--1

create user george identified by george;
grant dba  to george;
-- for access sth
--grant create <operation> to <username>

--2.1
set serveroutput on;
create or replace procedure P_PRINT_HELLO is
begin
--printing hello world
dbms_output.put_line('Hello , World !');
end P_PRINT_HELLO;
execute P_PRINT_HELLO;
-----------------------------------------------
--2.2
create or replace procedure P_CREATE_TAB_EMP is
begin
execute immediate '
CREATE TABLE EMP(
EMPNO NUMBER(4),
ENAME VARCHAR2(10),
JOB_TITLE VARCHAR2(9),
MGR NUMBER(4), --stands for manager 
HIREDATE DATE,
SAL NUMBER(7,2),
COMM NUMBER(7,2),
DEPTNO NUMBER(2),
CONSTRAINT PK_EMPNO PRIMARY KEY (EMPNO),
CONSTRAINT FK_DEPNO FOREIGN KEY (DEPTNO)
REFERENCES DEPT(DEPTNO)
)';
END P_CREATE_TAB_EMP;
/
execute P_CREATE_TAB_EMP;
-----------------------
--2.3
create or replace procedure P_CREATE_TAB_DEPT is
begin
execute immediate '
CREATE TABLE DEPT(
DEPTNO NUMBER(2),
DNAME VARCHAR2(14),
LOC VARCHAR2(13),
CONSTRAINT PK_DEPTNO PRIMARY KEY (DEPTNO)
)';
END P_CREATE_TAB_DEPT;
/
execute P_CREATE_TAB_DEPT;
--------------------------------------
--2.4
create or replace procedure P_INSERT_DATA is
begin
insert into dept(deptno,dname,loc)
values (10,'ACCOUNTING','NEW YORK');
insert into dept(deptno,dname,loc)
values
(20,'RESEARCH','DALLAS');
insert into dept(deptno,dname,loc)
values
(30,'SALES','CHICAGO');
insert into dept(deptno,dname,loc)
values
(40,'OPERATIONS','BOSTON');

insert into emp(empno,ename,job_title,mgr,hiredate,sal,comm,deptno)
values
(7369,'SMITH','CLERK',7902,to_date('17-DEC-80','DD-MON-YY'),800,NULL,20);
insert into emp(empno,ename,job_title,mgr,hiredate,sal,comm,deptno)
values
(7499,'ALLEN','SALESMAN',7698,to_date('20-FEB-81','DD-MON-YY'),1600,300,30);
insert into emp(empno,ename,job_title,mgr,hiredate,sal,comm,deptno)
values
(7521,'WARD','SALESMAN',7698, to_date('22-FEB-81','DD-MON-YY') ,1250,500,30);
insert into emp(empno,ename,job_title,mgr,hiredate,sal,comm,deptno)
values
(7566,'JONES','MANAGER',7839,to_date('02-APR-81','DD-MON-YY'),2975,NULL,20);
insert into emp(empno,ename,job_title,mgr,hiredate,sal,comm,deptno)
values
(7654,'MARTIN','SALESMAN',7698,to_date('28-SEP-81','DD-MON-YY'),1250,1400,30);
insert into emp(empno,ename,job_title,mgr,hiredate,sal,comm,deptno)
values
(7698,'BLAKE','MANAGER',7839,to_date('01-MAY-81','DD-MON-YY'), 2850,NULL,30);
insert into emp(empno,ename,job_title,mgr,hiredate,sal,comm,deptno)
values
(7782,'CLAR','MANAGER',7839,to_date('09-JUN-81','DD-MON-YY'),2450,NULL,10);
insert into emp(empno,ename,job_title,mgr,hiredate,sal,comm,deptno)
values
(7788,'SCOTT','ANALYST',7566,to_date('19-APR-78','DD-MON-YY'),3000,NULL,20);
insert into emp(empno,ename,job_title,mgr,hiredate,sal,comm,deptno)
values
(7839,'KING','PRESIDENT',NULL,to_date('17-NOV-81','DD-MON-YY'),5000,null,20);
insert into emp(empno,ename,job_title,mgr,hiredate,sal,comm,deptno)
values
(7844,'TURNER','SALESMAN',7698,to_date('08-SEP-81','DD-MON-YY'),1500,0,30);
insert into emp(empno,ename,job_title,mgr,hiredate,sal,comm,deptno)
values
(7876,'ADAMS','CLERK',7788,to_date('23-MAY-87','DD-MON-YY'),1100,NULL,20);
insert into emp(empno,ename,job_title,mgr,hiredate,sal,comm,deptno)
values
(7900,'JAMES','CLERK',7698,to_date('03-DEC-81','DD-MON-YY'),950,NULL,30);
insert into emp(empno,ename,job_title,mgr,hiredate,sal,comm,deptno)
values
(7902,'FORD','ANALYST',7566,to_date('03-DEC-81','DD-MON-YY'),3000,NULL,20);
insert into emp(empno,ename,job_title,mgr,hiredate,sal,comm,deptno)
values
(7934,'MILLER','CLERK',7782,to_date('23-JAN-82','DD-MON-YY'),1300,NULL,10);
end P_INSERT_DATA;
/
execute P_INSERT_DATA;
---------------------------------------------
--2.5
set serveroutput on;
create or replace procedure P_PRINT_EMP_SAL is 
cursor printempsal is select emp.ename,emp.sal,dept.dname from emp join dept on emp.deptno=dept.deptno;
var_print_empsal printempsal%rowtype;
begin 
open printempsal; 
loop
fetch printempsal into var_print_empsal;
exit when(printempsal%notfound);
dbms_output.put_line('Salary for Employee ' || var_print_empsal.ename || ' from Department ' || var_print_empsal.dname || ' = ' || var_print_empsal.sal);
end loop;
close printempsal;
end;
/
execute P_PRINT_EMP_SAL;
-----------------------------------------------
--2.6
create or replace procedure P_UPDATE_SAL is
empsalary number;
empnum number;
begin 
empsalary:='&empsalary';
empnum:='&empnum';
update emp set sal=empsalary where empno =empnum;
end P_UPDATE_SAL;
execute P_UPDATE_SAL;
---------------------------
--2.7
set serveroutput on;
create or replace procedure P_SQL_FUN is
cursor salmoney is select ename||lpad(' ',10-length(ename),' ')||lpad('*',(sal-0.001)/100+1,'*') as "EMPLOYEE_AND_THEIR_SALARIES" FROM emp order by sal desc;
var_data salmoney%rowtype;
begin
open salmoney;
fetch salmoney into var_data;
close salmoney;
end P_SQL_FUN;
execute P_SQL_FUN;
-----------------------------
2.8 
create or replace procedure P_DELETE_USER  is
begin
delete from emp where deptno=20;
end;
execute P_DELETE_USER;

//adding package

CREATE OR REPLACE PACKAGE george AS
   PROCEDURE P_PRINT_HELLO;
   PROCEDURE P_CREATE_TAB_EMP;
   PROCEDURE P_CREATE_TAB_DEPT;
   PROCEDURE P_INSERT_DATA;
   PROCEDURE P_PRINT_EMP_SAL;
   PROCEDURE P_UPDATE_SAL;
   PROCEDURE P_DELETE_USER;
END;
/
-----------------------------
--2.9 function
create or replace function F_DEPT_STAT (P_DEPT_ID IN number, STAT_ID IN Number , P_OUTPUT  out varchar2)
return varchar2
IS
deptname varchar2(100);
Begin
if stat_id = 1
then
SELECT Count(*) INTO P_OUTPUT FROM emp WHERE  emp.deptno = P_DEPT_ID;
select dname into deptname from dept where deptno = P_DEPT_ID;
DBMS_OUTPUT.PUT_LINE('Department ' || deptname || ' has total number of employee = ' || P_OUTPUT );
END if;
if stat_id = 2
then
SELECT AVG(sal) Into P_OUTPUT FROM emp WHERE deptno = P_DEPT_ID;
select dname into deptname from dept where deptno = P_DEPT_ID;
DBMS_OUTPUT.PUT_LINE('Department ' || deptname || ' has has average salary = ' || P_OUTPUT );
END IF;
return P_OUTPUT;
END F_DEPT_STAT;
-----------------------------
--3
create database link geo_link
connect to georgemusa97 identified by AbC123456788
USING '172.17.0.1:11521/ee'

--------------------------------

--4

create materialized view HRdepartment as 
select
    e.deptno department,
    e.ename employee,
    c.ename colleague
from
    emp e,
    emp c
where
    e.deptno = c.deptno
and
    e.ename != c.ename
ORDER BY e.deptno, e.ename, c.ename;
------------------------------------------------------
--5

begin
      dbms_scheduler.ENABLE(NAME=>'"GEORGE"."JOB_5"');
end;
--putting the time inside the box
--FREQ=MINUTELY;INTERVAL=5;BYDAY=SUN,MON,TUE,WED,THU
------------------------------------------------------
--6
create table PS_ORDERS( emp_id number(5) ,  QUANTITY number(4),  COST_PER_ITEM number(6,2),  TOTAL_COST number(8,2),  CREATE_DATE date,  CREATED_BY varchar2(10), 
UPDATE_DATE date, 
UPDATED_BY varchar2(10));

------------------------------

--6.1
create sequence usr_seq
start with 1
increment by 1
nomaxvalue;

create or replace trigger t_insert_auto_value before insert
ON ps_orders
for each row
begin
select usr_seq.nextval 
into :new.id from dual;
end;
--------------------------------------
--6.2
create or replace trigger time_data_t before insert on ps_orders for each row
begin
:new.create_date:=sysdate;
end;
-------------------------------------------
--6.3
create table auditor (user_name varchar2(20), event varchar2(20));
create or replace trigger insert_audit after insert on ps_orders
declare v_user_name varchar2(20);
begin
select user into v_user_name from dual;
if inserting then 
insert into auditor values(v_user_name,'inserting');
end if;
end;
--------------------------------------------
--6.4

create or replace trigger cost_t before insert on ps_orders for each row   
begin
:new.total_cost:= :new.quantity *: new.cost_per_item;
end;
----------------------------------
--6.5
create table auditor2 (update_date date,v_user_up varchar2(20));

create or replace trigger update_audit after update on ps_orders 
declare v_userupdate varchar2(20);
begin
select user into v_userupdate from dual;
if updating then 
insert into auditor2 values(sysdate,v_userupdate);
end if;
end;
-----------------------------
--7
--create table
CREATE TABLE player_info(
    player_id number,
    full_name varchar2(50) not null,
    gender varchar(20),
    date_of_reg date not null,
    loc VARCHAR2(255) not null,
    constraint pk_id primary key(player_id)
);

INSERT INTO "GEORGE"."PLAYER_INFO" (PLAYER_ID, FULL_NAME, GENDER, DATE_OF_REG, LOC) VALUES ('1', 'Yazeed Kh', 'Male', TO_DATE('2020-09-01 10:28:04', 'YYYY-MM-DD HH24:MI:SS'), 'Amman')
INSERT INTO "GEORGE"."PLAYER_INFO" (PLAYER_ID, FULL_NAME, GENDER, DATE_OF_REG, LOC) VALUES ('2', 'Khaled Atta', 'Male', TO_DATE('2019-11-01 10:28:36', 'YYYY-MM-DD HH24:MI:SS'), 'Kuwait')
INSERT INTO "GEORGE"."PLAYER_INFO" (PLAYER_ID, FULL_NAME, GENDER, DATE_OF_REG, LOC) VALUES ('3', 'Raya AlNabulsi', 'Female', TO_DATE('2021-01-07 10:29:49', 'YYYY-MM-DD HH24:MI:SS'), 'Berlin')
INSERT INTO "GEORGE"."PLAYER_INFO" (PLAYER_ID, FULL_NAME, GENDER, DATE_OF_REG, LOC) VALUES ('4', 'George Almousa', 'Male', TO_DATE('2014-06-10 10:31:37', 'YYYY-MM-DD HH24:MI:SS'), 'Tokyo')

--alter table

alter table player_info add email varchar(50);

UPDATE "GEORGE"."PLAYER_INFO" SET EMAIL = 'kh@mail.com' WHERE ROWID = 'AAASAKAAHAAAAIsAAA' AND ORA_ROWSCN = '2376335'
UPDATE "GEORGE"."PLAYER_INFO" SET EMAIL = 'ka@mail.com' WHERE ROWID = 'AAASAKAAHAAAAIsAAB' AND ORA_ROWSCN = '2376335'
UPDATE "GEORGE"."PLAYER_INFO" SET EMAIL = 'raya@mail.com' WHERE ROWID = 'AAASAKAAHAAAAIsAAC' AND ORA_ROWSCN = '2376335'
UPDATE "GEORGE"."PLAYER_INFO" SET EMAIL = 'gm@mail.com' WHERE ROWID = 'AAASAKAAHAAAAIsAAD' AND ORA_ROWSCN = '2376335'
-------------------------------------------
--7.1
--B-Tree
create index first_index on player_info(loc) invisible;
--------------------------------------------
--7.2
-- bitmap
create bitmap index second_index on player_info(gender);
--------------------------------------------
--7.3
--unique
create unique index third_index on player_info(email);
--------------------------------------------
--7.4
--Function base
create index fourth_index on player_info(lower(full_name));
--------------------------------------------
--7.5
--Concatenated
create index fifth_index on player_info(full_name,loc);
---------------------------------------------


